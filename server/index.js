'use strict'

const Koa = require('koa')
const next = require('next')
const Router = require('koa-router')

const PORT = process.env.PORT || 3000
const dev = process.env.NODE_ENV !== 'production'

const app = next({ dev })
const handle = app.getRequestHandler()

app.prepare().then(() => {
  const server = new Koa()
  const router = new Router()

  // apply router middleware
  router.get('*', nextJsAppMiddleware)

  // apply server middleware
  server.use(applyHttpStatusMiddleware)
  server.use(router.routes())

  // start the server
  server.listen(PORT, (err) => {
    if (err) throw err
    console.log(`> Ready on http://localhost:${PORT}`)
  })
})
.catch(err => {
  console.error('Unrecoverable error: %s', err.stack)
})

async function applyHttpStatusMiddleware(ctx, next) {
  ctx.res.statusCode = 200
  await next()
}

async function nextJsAppMiddleware(ctx) {
  await handle(ctx.req, ctx.res)
  ctx.respond = false
}